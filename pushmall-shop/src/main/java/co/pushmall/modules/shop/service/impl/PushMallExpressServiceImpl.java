package co.pushmall.modules.shop.service.impl;

import co.pushmall.exception.EntityExistException;
import co.pushmall.modules.shop.domain.PushMallExpress;
import co.pushmall.modules.shop.repository.PushMallExpressRepository;
import co.pushmall.modules.shop.service.PushMallExpressService;
import co.pushmall.modules.shop.service.dto.PushMallExpressDTO;
import co.pushmall.modules.shop.service.dto.PushMallExpressQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallExpressMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-12
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallExpressServiceImpl implements PushMallExpressService {

    private final PushMallExpressRepository pushMallExpressRepository;

    private final PushMallExpressMapper pushMallExpressMapper;

    public PushMallExpressServiceImpl(PushMallExpressRepository pushMallExpressRepository, PushMallExpressMapper pushMallExpressMapper) {
        this.pushMallExpressRepository = pushMallExpressRepository;
        this.pushMallExpressMapper = pushMallExpressMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallExpressQueryCriteria criteria, Pageable pageable) {
        Page<PushMallExpress> page = pushMallExpressRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallExpressMapper::toDto));
    }

    @Override
    public List<PushMallExpressDTO> queryAll(PushMallExpressQueryCriteria criteria) {
        return pushMallExpressMapper.toDto(pushMallExpressRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallExpressDTO findById(Integer id) {
        Optional<PushMallExpress> pushMallExpress = pushMallExpressRepository.findById(id);
        //ValidationUtil.isNull(pushMallExpress,"PushMallExpress","id",id);
        if (pushMallExpress.isPresent()) {
            return pushMallExpressMapper.toDto(pushMallExpress.get());
        }
        return new PushMallExpressDTO();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallExpressDTO create(PushMallExpress resources) {
        if (pushMallExpressRepository.findByCode(resources.getCode()) != null) {
            throw new EntityExistException(PushMallExpress.class, "code", resources.getCode());
        }
        return pushMallExpressMapper.toDto(pushMallExpressRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallExpress resources) {
        Optional<PushMallExpress> optionalPushMallExpress = pushMallExpressRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallExpress, "PushMallExpress", "id", resources.getId());
        PushMallExpress pushMallExpress = optionalPushMallExpress.get();
        PushMallExpress pushMallExpress1 = null;
        pushMallExpress1 = pushMallExpressRepository.findByCode(resources.getCode());
        if (pushMallExpress1 != null && !pushMallExpress1.getId().equals(pushMallExpress.getId())) {
            throw new EntityExistException(PushMallExpress.class, "code", resources.getCode());
        }
        pushMallExpress.copy(resources);
        pushMallExpressRepository.save(pushMallExpress);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallExpressRepository.deleteById(id);
    }
}
