package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallSystemUserTask;
import co.pushmall.modules.shop.repository.PushMallSystemUserTaskRepository;
import co.pushmall.modules.shop.service.PushMallSystemUserLevelService;
import co.pushmall.modules.shop.service.PushMallSystemUserTaskService;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserTaskDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserTaskQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallSystemUserTaskMapper;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-04
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallSystemUserTaskServiceImpl implements PushMallSystemUserTaskService {

    private final PushMallSystemUserTaskRepository pushMallSystemUserTaskRepository;

    private final PushMallSystemUserTaskMapper pushMallSystemUserTaskMapper;

    private final PushMallSystemUserLevelService systemUserLevelService;

    public PushMallSystemUserTaskServiceImpl(PushMallSystemUserTaskRepository pushMallSystemUserTaskRepository, PushMallSystemUserTaskMapper pushMallSystemUserTaskMapper, PushMallSystemUserLevelService systemUserLevelService) {
        this.pushMallSystemUserTaskRepository = pushMallSystemUserTaskRepository;
        this.pushMallSystemUserTaskMapper = pushMallSystemUserTaskMapper;
        this.systemUserLevelService = systemUserLevelService;
    }

    @Override
    public Map<String, Object> queryAll(PushMallSystemUserTaskQueryCriteria criteria, Pageable pageable) {

        Page<PushMallSystemUserTask> page = pushMallSystemUserTaskRepository
                .findAll((root, criteriaQuery, criteriaBuilder)
                        -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);

        List<PushMallSystemUserTaskDTO> systemUserTaskDTOS = pushMallSystemUserTaskMapper
                .toDto(page.getContent());
        for (PushMallSystemUserTaskDTO systemUserTaskDTO : systemUserTaskDTOS) {
            systemUserTaskDTO.setLevalName(systemUserLevelService
                    .findById(systemUserTaskDTO.getLevelId()).getName());
        }

        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", systemUserTaskDTOS);
        map.put("totalElements", page.getTotalElements());

        return map;

    }

    @Override
    public List<PushMallSystemUserTaskDTO> queryAll(PushMallSystemUserTaskQueryCriteria criteria) {
        return pushMallSystemUserTaskMapper.toDto(pushMallSystemUserTaskRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallSystemUserTaskDTO findById(Integer id) {
        Optional<PushMallSystemUserTask> yxSystemUserTask = pushMallSystemUserTaskRepository.findById(id);
        //ValidationUtil.isNull(yxSystemUserTask,"PushMallSystemUserTask","id",id);
        if (yxSystemUserTask.isPresent()) {
            return pushMallSystemUserTaskMapper.toDto(yxSystemUserTask.get());
        }
        return new PushMallSystemUserTaskDTO();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallSystemUserTaskDTO create(PushMallSystemUserTask resources) {
        return pushMallSystemUserTaskMapper.toDto(pushMallSystemUserTaskRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallSystemUserTask resources) {
        Optional<PushMallSystemUserTask> optionalPushMallSystemUserTask = pushMallSystemUserTaskRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallSystemUserTask, "PushMallSystemUserTask", "id", resources.getId());
        PushMallSystemUserTask pushMallSystemUserTask = optionalPushMallSystemUserTask.get();
        pushMallSystemUserTask.copy(resources);
        pushMallSystemUserTaskRepository.save(pushMallSystemUserTask);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallSystemUserTaskRepository.deleteById(id);
    }

    /**
     * 任务类型
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> getTaskType() {
        List<Map<String, Object>> list = null;
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("type", "SatisfactionIntegral");
        map.put("type", "SatisfactionIntegral");
        map.put("type", "SatisfactionIntegral");
        map.put("type", "SatisfactionIntegral");
        map.put("type", "SatisfactionIntegral");
        map.put("type", "SatisfactionIntegral");
        return null;
    }
}
