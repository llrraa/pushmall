package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.service.PushMallUserBillService;
import co.pushmall.modules.shop.service.dto.PushMallUserBillQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-06
 */
@Api(tags = "商城:用户账单管理")
@RestController
@RequestMapping("api")
public class UserBillController {

    private final PushMallUserBillService pushMallUserBillService;

    public UserBillController(PushMallUserBillService pushMallUserBillService) {
        this.pushMallUserBillService = pushMallUserBillService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmUserBill")
    @PreAuthorize("@el.check('admin','YXUSERBILL_ALL','YXUSERBILL_SELECT')")
    public ResponseEntity getPushMallUserBills(PushMallUserBillQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallUserBillService.queryAll(criteria, pageable), HttpStatus.OK);
    }


}
