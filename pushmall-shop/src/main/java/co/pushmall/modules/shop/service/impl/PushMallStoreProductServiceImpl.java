package co.pushmall.modules.shop.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import co.pushmall.exception.BadRequestException;
import co.pushmall.modules.activity.domain.PushMallStoreDiscount;
import co.pushmall.modules.activity.repository.PushMallStoreDiscountRepository;
import co.pushmall.modules.activity.service.mapper.PushMallStoreDiscountMapper;
import co.pushmall.modules.shop.domain.PushMallStoreCategory;
import co.pushmall.modules.shop.domain.PushMallStoreProduct;
import co.pushmall.modules.shop.domain.PushMallStoreProductAttr;
import co.pushmall.modules.shop.domain.PushMallStoreProductAttrResult;
import co.pushmall.modules.shop.domain.PushMallStoreProductAttrValue;
import co.pushmall.modules.shop.domain.PushMallSystemUserLevel;
import co.pushmall.modules.shop.repository.PushMallStoreCategoryRepository;
import co.pushmall.modules.shop.repository.PushMallStoreProductAttrRepository;
import co.pushmall.modules.shop.repository.PushMallStoreProductAttrResultRepository;
import co.pushmall.modules.shop.repository.PushMallStoreProductAttrValueRepository;
import co.pushmall.modules.shop.repository.PushMallStoreProductRepository;
import co.pushmall.modules.shop.repository.PushMallSystemUserLevelRepository;
import co.pushmall.modules.shop.service.PushMallStoreProductService;
import co.pushmall.modules.shop.service.dto.DetailDTO;
import co.pushmall.modules.shop.service.dto.FromatDetailDTO;
import co.pushmall.modules.shop.service.dto.ProductFormatDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallStoreProductMapper;
import co.pushmall.utils.FileUtil;
import co.pushmall.utils.OrderUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.StringUtils;
import co.pushmall.utils.ValidationUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static co.pushmall.utils.OrderUtil.dateToTimestamp;

/**
 * @author pushmall
 * @date 2019-10-04
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreProductServiceImpl implements PushMallStoreProductService {

    private final PushMallStoreProductRepository pushMallStoreProductRepository;
    private final PushMallStoreProductAttrRepository pushMallStoreProductAttrRepository;
    private final PushMallStoreProductAttrValueRepository pushMallStoreProductAttrValueRepository;
    private final PushMallStoreProductAttrResultRepository pushMallStoreProductAttrResultRepository;
    private final PushMallStoreCategoryRepository pushMallStoreCategoryRepository;
    private final PushMallSystemUserLevelRepository pushMallSystemUserLevelRepository;
    private final PushMallStoreDiscountRepository pushMallStoreDiscountRepository;
    private final PushMallStoreDiscountMapper pushMallStoreDiscountMapper;

    private final PushMallStoreProductMapper pushMallStoreProductMapper;

    public PushMallStoreProductServiceImpl(PushMallStoreProductRepository pushMallStoreProductRepository,
                                           PushMallStoreProductAttrRepository pushMallStoreProductAttrRepository, PushMallStoreProductAttrValueRepository pushMallStoreProductAttrValueRepository,
                                           PushMallStoreProductAttrResultRepository pushMallStoreProductAttrResultRepository, PushMallStoreProductMapper pushMallStoreProductMapper, PushMallStoreCategoryRepository pushMallStoreCategoryRepository,
                                           PushMallSystemUserLevelRepository pushMallSystemUserLevelRepository, PushMallStoreDiscountRepository pushMallStoreDiscountRepository, PushMallStoreDiscountMapper pushMallStoreDiscountMapper) {
        this.pushMallStoreProductRepository = pushMallStoreProductRepository;
        this.pushMallStoreProductAttrRepository = pushMallStoreProductAttrRepository;
        this.pushMallStoreProductAttrValueRepository = pushMallStoreProductAttrValueRepository;
        this.pushMallStoreProductAttrResultRepository = pushMallStoreProductAttrResultRepository;
        this.pushMallStoreProductMapper = pushMallStoreProductMapper;
        this.pushMallStoreCategoryRepository = pushMallStoreCategoryRepository;
        this.pushMallSystemUserLevelRepository = pushMallSystemUserLevelRepository;
        this.pushMallStoreDiscountRepository = pushMallStoreDiscountRepository;
        this.pushMallStoreDiscountMapper = pushMallStoreDiscountMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreProductQueryCriteria criteria, Pageable pageable) {
        //criteria.setIsDel(0);
        Page<PushMallStoreProduct> page = pushMallStoreProductRepository
                .findAll((root, criteriaQuery, criteriaBuilder)
                                -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)
                        , pageable);
        List<PushMallStoreProductDTO> storeProductDTOS = new ArrayList<>();
        for (PushMallStoreProduct product : page.getContent()) {

            PushMallStoreProductDTO pushMallStoreProductDTO = pushMallStoreProductMapper.toDto(product);
            //规格属性库存
            Integer newStock = pushMallStoreProductAttrValueRepository.sumStock(product.getId());
            if (newStock != null) {
                pushMallStoreProductDTO.setStock(newStock);
            }
            //设置商品最低价格
            BigDecimal lowestPrice = pushMallStoreProductAttrValueRepository.lowestPrice(product.getId());
            if (lowestPrice != null) {
                pushMallStoreProductDTO.setPrice(lowestPrice);
            }

            storeProductDTOS.add(pushMallStoreProductDTO);
        }
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", storeProductDTOS);
        map.put("totalElements", page.getTotalElements());
        return map;
    }

    @Override
    public List<PushMallStoreProductDTO> queryAll(PushMallStoreProductQueryCriteria criteria) {
        return pushMallStoreProductMapper.toDto(pushMallStoreProductRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreProductDTO findById(Integer id) {
        Optional<PushMallStoreProduct> yxStoreProduct = pushMallStoreProductRepository.findById(id);
        //ValidationUtil.isNull(yxStoreProduct,"PushMallStoreProduct","id",id);
        if (yxStoreProduct.isPresent()) {
            return pushMallStoreProductMapper.toDto(yxStoreProduct.get());
        }
        return new PushMallStoreProductDTO();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreProductDTO create(PushMallStoreProduct resources) {
        return pushMallStoreProductMapper.toDto(pushMallStoreProductRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreProduct resources) {
        Optional<PushMallStoreProduct> optionalPushMallStoreProduct = pushMallStoreProductRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreProduct, "PushMallStoreProduct", "id", resources.getId());
        PushMallStoreProduct pushMallStoreProduct = optionalPushMallStoreProduct.get();
        pushMallStoreProduct.copy(resources);
        pushMallStoreProductRepository.save(pushMallStoreProduct);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreProductRepository.updateDel(1, id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void recovery(Integer id) {
        pushMallStoreProductRepository.updateDel(0, id);
        pushMallStoreProductRepository.updateOnsale(0, id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void onSale(Integer id, Integer status) {
        if (status == 1) {
            status = 0;
        } else {
            status = 1;
        }
        pushMallStoreProductRepository.updateOnsale(status, id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<ProductFormatDTO> isFormatAttr(Integer id, String jsonStr) {
        if (ObjectUtil.isNull(id)) {
            throw new BadRequestException("产品不存在");
        }

        PushMallStoreProductDTO pushMallStoreProductDTO = findById(id);

        DetailDTO detailDTO = attrFormat(jsonStr);

        //System.out.println("list:"+detailDTO.getRes());
        List<ProductFormatDTO> newList = new ArrayList<>();
        for (Map<String, Map<String, String>> map : detailDTO.getRes()) {
            ProductFormatDTO productFormatDTO = new ProductFormatDTO();

            productFormatDTO.setDetail(map.get("detail"));
            productFormatDTO.setCost(pushMallStoreProductDTO.getCost().doubleValue());
            productFormatDTO.setPrice(pushMallStoreProductDTO.getPrice().doubleValue());
            productFormatDTO.setSales(pushMallStoreProductDTO.getSales());
            productFormatDTO.setPic(pushMallStoreProductDTO.getImage());
            productFormatDTO.setWholesale(new Double(0.00));
            productFormatDTO.setBarCode("");
            productFormatDTO.setPackaging(pushMallStoreProductDTO.getPackaging());
            productFormatDTO.setCheck(false);
            newList.add(productFormatDTO);
        }
        return newList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createProductAttr(Integer id, String jsonStr) {
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        String items = jsonObject.get("items") == null ? jsonObject.get("attr").toString() : JSON.toJSONString(jsonObject.get("items"));
        String attrs = jsonObject.get("attrs") == null ? jsonObject.get("value").toString() : JSON.toJSONString(jsonObject.get("attrs"));
        //System.out.println(jsonObject);
        List<FromatDetailDTO> attrList = JSON.parseArray(items,
                FromatDetailDTO.class);
        List<ProductFormatDTO> valueList = JSON.parseArray(attrs,
                ProductFormatDTO.class);

        List<PushMallStoreProductAttr> attrGroup = new ArrayList<>();
        for (FromatDetailDTO fromatDetailDTO : attrList) {
            PushMallStoreProductAttr pushMallStoreProductAttr = new PushMallStoreProductAttr();
            pushMallStoreProductAttr.setProductId(id);
            pushMallStoreProductAttr.setAttrName(fromatDetailDTO.getValue());
            pushMallStoreProductAttr.setAttrValues(StrUtil.
                    join(",", fromatDetailDTO.getDetail()));
            attrGroup.add(pushMallStoreProductAttr);
        }


        List<PushMallStoreProductAttrValue> valueGroup = new ArrayList<>();
        for (ProductFormatDTO productFormatDTO : valueList) {
            PushMallStoreProductAttrValue pushMallStoreProductAttrValue = new PushMallStoreProductAttrValue();
            pushMallStoreProductAttrValue.setProductId(id);
            //productFormatDTO.getDetail().values().stream().collect(Collectors.toList());
            List<String> stringList = productFormatDTO.getDetail().values()
                    .stream().collect(Collectors.toList());
            Collections.sort(stringList);
            pushMallStoreProductAttrValue.setSuk(StrUtil.
                    join(",", stringList));
            pushMallStoreProductAttrValue.setPrice(BigDecimal.valueOf(productFormatDTO.getPrice()));
            pushMallStoreProductAttrValue.setCost(BigDecimal.valueOf(productFormatDTO.getCost()));
            pushMallStoreProductAttrValue.setStock(productFormatDTO.getSales());
            pushMallStoreProductAttrValue.setUnique(IdUtil.simpleUUID());
            pushMallStoreProductAttrValue.setImage(productFormatDTO.getPic());
            pushMallStoreProductAttrValue.setWholesale(ObjectUtils.isEmpty(productFormatDTO.getWholesale()) ? new BigDecimal(0) : BigDecimal.valueOf(productFormatDTO.getWholesale()));
            pushMallStoreProductAttrValue.setBarCode(productFormatDTO.getBarCode());
            pushMallStoreProductAttrValue.setPackaging(productFormatDTO.getPackaging());

            valueGroup.add(pushMallStoreProductAttrValue);
        }

        if (attrGroup.isEmpty() || valueGroup.isEmpty()) {
            throw new BadRequestException("请设置至少一个属性!");
        }

        //插入之前清空
        clearProductAttr(id, true);


        //保存属性
        pushMallStoreProductAttrRepository.saveAll(attrGroup);

        //保存值
        pushMallStoreProductAttrValueRepository.saveAll(valueGroup);

        //设置商品最低价格
        BigDecimal lowestPrice = pushMallStoreProductAttrValueRepository.lowestPrice(id);
        if (lowestPrice != null) {
            pushMallStoreProductRepository.updatePrice(lowestPrice, id);
        }

        //设置商品库存
        Integer stock = pushMallStoreProductAttrValueRepository.sumStock(id);
        if (stock != null) {
            pushMallStoreProductRepository.updateStock(stock, id);
        }

        Map<String, Object> map = new LinkedHashMap<>();
        Object attr = ObjectUtil.isNull(jsonObject.get("items")) ? jsonObject.get("attr") : jsonObject.get("items");
        Object value = ObjectUtil.isNull(jsonObject.get("attrs")) ? jsonObject.get("value") : jsonObject.get("attrs");
        map.put("attr", attr);
        map.put("value", value);

        //保存结果
        setResult(map, id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setResult(Map<String, Object> map, Integer id) {
        PushMallStoreProductAttrResult pushMallStoreProductAttrResult = new PushMallStoreProductAttrResult();
        pushMallStoreProductAttrResult.setProductId(id);
        pushMallStoreProductAttrResult.setResult(JSON.toJSONString(map));
        pushMallStoreProductAttrResult.setChangeTime(OrderUtil.getSecondTimestampTwo());

        pushMallStoreProductAttrResultRepository.deleteByProductId(id);

        pushMallStoreProductAttrResultRepository.save(pushMallStoreProductAttrResult);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void clearProductAttr(Integer id, boolean isActice) {
        if (ObjectUtil.isNull(id)) {
            throw new BadRequestException("产品不存在");
        }

        pushMallStoreProductAttrRepository.deleteByProductId(id);
        pushMallStoreProductAttrValueRepository.deleteByProductId(id);

        if (isActice) {
            pushMallStoreProductAttrResultRepository.deleteByProductId(id);
        }
    }

    @Override
    public String getStoreProductAttrResult(Integer id) {
        PushMallStoreProductAttrResult pushMallStoreProductAttrResult = pushMallStoreProductAttrResultRepository
                .findByProductId(id);
        if (ObjectUtil.isNull(pushMallStoreProductAttrResult)) {
            return "";
        }
        return pushMallStoreProductAttrResult.getResult();
    }

    @Override
    public void download(List<PushMallStoreProductDTO> queryAll, HttpServletResponse response) throws IOException, ParseException {
        List<Map<String, Object>> list = new ArrayList<>();
        List<PushMallSystemUserLevel> pushMallSystemUserLevel = pushMallSystemUserLevelRepository.findAll();
        Integer time = OrderUtil.dateToTimestamp(new Date());

        if (queryAll != null && queryAll.size() > 0) {
            for (PushMallStoreProductDTO pushMallStoreProductDTO : queryAll) {
                String storeProductAttrResult = getStoreProductAttrResult(pushMallStoreProductDTO.getId());
                JSONObject jsonObject = JSON.parseObject(storeProductAttrResult);
                JSONArray jsonAttr = JSONArray.parseArray(jsonObject != null ? jsonObject.get("attr").toString() : "");
                JSONArray jsonValue = JSONArray.parseArray(jsonObject != null ? jsonObject.get("value").toString() : "");

                List nameArr = new ArrayList();
                List typeArr = new ArrayList();
                if (jsonAttr != null) {
                    for (Object json : jsonAttr) {
                        JSONObject jsonO = JSON.parseObject(json.toString());
                        nameArr.add(jsonO != null ? jsonO.get("value") : "");
                        List detail = JSON.parseArray(jsonO.get("detail").toString(), String.class);
                        typeArr.add(jsonO != null ? StringUtils.join(detail.toArray(), ",") : "");
                    }
                }

                List priceArr = new ArrayList();
                List costArr = new ArrayList();
                List stockArr = new ArrayList();
                List picArr = new ArrayList();
                List barCodeArr = new ArrayList();
                List packagingArr = new ArrayList();
                if (jsonValue != null) {
                    for (Object json : jsonValue) {
                        JSONObject jsonO = JSON.parseObject(json.toString());
                        priceArr.add(jsonO != null ? jsonO.get("price") : 0);
                        costArr.add(jsonO != null ? jsonO.get("cost") : 0);
                        stockArr.add(jsonO != null ? jsonO.get("sales") : 0);
                        picArr.add(jsonO != null ? jsonO.get("pic") : "@");
                        barCodeArr.add(jsonO != null ? jsonO.get("barCode") : "0");
                        packagingArr.add(jsonO != null ? jsonO.get("packaging") : "0");
                    }
                }

                Map<String, Object> map = new LinkedHashMap<>();
                map.put("商品ID", pushMallStoreProductDTO.getId());

                List<PushMallStoreCategory> pushMallStoreCategoryList = pushMallStoreCategoryRepository.findParentsById(pushMallStoreProductDTO.getStoreCategory().getId());
                // 一级分类
                map.put("栏目分类", pushMallStoreCategoryList.size() >= 1 ? pushMallStoreCategoryList.get(0).getCateName() : "");
                // 二级分类
                map.put("商品分类", pushMallStoreCategoryList.size() >= 2 ? pushMallStoreCategoryList.get(1).getCateName() : "");
                // 系列
                map.put("商品系列", pushMallStoreCategoryList.size() >= 3 ? pushMallStoreCategoryList.get(2).getCateName() : "");
                map.put("商品名称", pushMallStoreProductDTO.getStoreName());
                map.put("商品规格", StringUtils.join(nameArr.toArray(), "/"));
                map.put("商品型号", StringUtils.join(typeArr.toArray(), "/"));
                map.put("商品编码", StringUtils.join(barCodeArr.toArray(), "/"));
                map.put("包装规格", StringUtils.join(packagingArr.toArray(), "/"));

                map.put("关键字", pushMallStoreProductDTO.getKeyword());
                // map.put("商品包装", pushMallStoreProductDTO.getPackaging());
                map.put("单位名称", pushMallStoreProductDTO.getUnitName());
                map.put("商品简介", pushMallStoreProductDTO.getStoreInfo());
                map.put("产品描述", pushMallStoreProductDTO.getDescription());
                map.put("邮费", pushMallStoreProductDTO.getPostage());
                map.put("排序", pushMallStoreProductDTO.getSort());
                map.put("销量", pushMallStoreProductDTO.getSales());
                map.put("热卖单品", pushMallStoreProductDTO.getIsHot());
                map.put("促销单品", pushMallStoreProductDTO.getIsBenefit());
                map.put("精品推荐", pushMallStoreProductDTO.getIsBest());
                map.put("首发新品", pushMallStoreProductDTO.getIsNew());
                map.put("是否包邮", pushMallStoreProductDTO.getIsPostage());
                map.put("优品推荐", pushMallStoreProductDTO.getIsGood());
                map.put("获得积分", pushMallStoreProductDTO.getGiveIntegral());
                map.put("虚拟销量", pushMallStoreProductDTO.getFicti());
                map.put("是否上架", pushMallStoreProductDTO.getIsShow());

                // 商品成本价
                String costStr = costArr.size() > 0 ? StringUtils.join(costArr.toArray(), "/") : "";
                // 商品零售价
                Double[] numArr = new Double[priceArr.size()];
                for (int c = 0; c < costArr.size(); c++) {
                    Double num = 1.00;
                    if (priceArr.size() > 0 && costArr.size() == priceArr.size()) {
                        Double cost = Double.valueOf(costArr.get(c).toString());
                        Double price = Double.valueOf(priceArr.get(c).toString());
                        num = cost == 0 || price == 0 ? 1.00 : cost / price;
                        numArr[c] = (double) Math.round(num * 100) / 100;
                    } else {
                        throw new BadRequestException("商品成本价或商品零售价数据异常!");
                    }
                }
                map.put("商品成本价", costStr);
                map.put("商品零售价基数", numArr.length > 0 ? StringUtils.join(numArr, "/") : 1);
                map.put("商品库存", stockArr.size() > 0 ? StringUtils.join(stockArr.toArray(), "/") : "");
                map.put("商品图片", picArr.size() > 0 ? StringUtils.join(picArr.toArray(), "&*&") : "");

                for (int m = 0; m < pushMallSystemUserLevel.size(); m++) {
                    List<PushMallStoreDiscount> pushMallStoreDiscount = pushMallStoreDiscountRepository.findByProductIdAndGrade(pushMallStoreProductDTO.getId(), pushMallSystemUserLevel.get(m).getGrade(), time);
                    map.put(pushMallSystemUserLevel.get(m).getName() + "折扣(%)", pushMallStoreDiscount != null && pushMallStoreDiscount.size() > 0 ? pushMallStoreDiscount.get(0).getDiscount() : 100);
                }

                list.add(map);
            }
        } else {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("商品ID", "");
            map.put("栏目分类", "");
            map.put("商品分类", "");
            map.put("商品系列", "");
            map.put("商品名称", "");
            map.put("商品规格", "");
            map.put("商品型号", "");
            map.put("商品编码", "");
            map.put("包装规格", "");
            map.put("关键字", "");
            // map.put("商品包装", "");
            map.put("单位名称", "");
            map.put("商品简介", "");
            map.put("产品描述", "");
            map.put("邮费", 0);
            map.put("排序", 0);
            map.put("销量", 0);
            map.put("热卖单品", 0);
            map.put("促销单品", 0);
            map.put("精品推荐", 0);
            map.put("首发新品", 0);
            map.put("是否包邮", 0);
            map.put("优品推荐", 0);
            map.put("获得积分", 0);
            map.put("虚拟销量", 0);
            map.put("是否上架", 0);
            map.put("商品成本价", 0);
            map.put("商品零售价基数", 0);
            map.put("商品库存", 0);
            map.put("商品图片", "&*&");

            for (PushMallSystemUserLevel level : pushMallSystemUserLevel) {
                map.put(level.getName() + "折扣(%)", 100);
            }

            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public int excelToList(InputStream inputStream) throws InvalidFormatException, IOException {
        List<PushMallStoreProductDTO> gisList = new ArrayList<PushMallStoreProductDTO>();
        Workbook workbook = null;
        workbook = WorkbookFactory.create(inputStream);
        inputStream.close();
        //工作表对象
        Sheet sheet = workbook.getSheetAt(0);
        //总行数
        int rowLength = sheet.getLastRowNum() + 1;
        //工作表的列
        Row row = sheet.getRow(0);
        //总列数
        int colLength = row.getLastCellNum();

        for (int i = 1; i < rowLength; i++) {
            row = sheet.getRow(i);
            for (int j = 0; j < colLength; j++) {
                if (row.getCell(j) != null) {
                    row.getCell(j).setCellType(CellType.STRING);
                } else {
                    row.createCell(j).setCellValue("0");
                }
            }
            PushMallStoreProduct resources = new PushMallStoreProduct();
            int product_id = 0;
            boolean aNull = ObjectUtil.isNotEmpty(row.getCell(0).getStringCellValue());
            if (aNull) {
                try {
                    product_id = Integer.parseInt(row.getCell(0).getStringCellValue());
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品ID不能包含字母、汉字及特殊字符, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }
            } else {
                product_id = (int) (Math.random() * (352324 + 1));
            }
            // 商品ID
            resources.setId(product_id);
            // 商品分类
            String name = StringUtils.isNotEmpty(row.getCell(3).getStringCellValue()) ?
                    row.getCell(3).getStringCellValue() :
                    StringUtils.isNotEmpty(row.getCell(2).getStringCellValue()) ?
                            row.getCell(2).getStringCellValue() :
                            row.getCell(1).getStringCellValue();
            PushMallStoreCategory pushMallStoreCategory = null;
            try {
                pushMallStoreCategory = pushMallStoreCategoryRepository.findByName(name);
            } catch (Exception e) {
                e.printStackTrace();
                String msg = " 商品分类查询信息异常, " + " 请检查商品分类，重新填写再次上传！ ";
                throw new BadRequestException(msg);
            }

            if (ObjectUtil.isNull(pushMallStoreCategory)) {
                String msg = " 商品分类【" + name + "】不存在, " + " 请重新填写再次上传！ ";
                throw new BadRequestException(msg);
            }
            resources.setStoreCategory(pushMallStoreCategory);
            // 商品系列
            // resources.setStoreName(row.getCell(2).getStringCellValue());
            // 商品名称
            resources.setStoreName(row.getCell(4).getStringCellValue());
            // 商品规格
            // resources.setStoreName(row.getCell(5).getStringCellValue());
            // 商品型号
            // resources.setStoreName(row.getCell(6).getStringCellValue());
            // 商品编码
            // resources.setBarCode(row.getCell(7).getStringCellValue());
            // 包装规格
            // resources.setPackaging(row.getCell(8).getStringCellValue());
            // 关键字
            resources.setKeyword(row.getCell(9).getStringCellValue());
            // 商品包装
            resources.setPackaging("");
            // 单位名
            resources.setUnitName(row.getCell(10).getStringCellValue());
            // 商品简介
            resources.setStoreInfo(row.getCell(11).getStringCellValue());
            // 产品描述
            resources.setDescription(row.getCell(12).getStringCellValue());
            // 邮费
            resources.setPostage(new BigDecimal(Double.parseDouble(row.getCell(13).getStringCellValue())));
            // 排序
            resources.setSort(Integer.parseInt(row.getCell(14).getStringCellValue()));
            // 销量
            resources.setSales(Integer.parseInt(row.getCell(15).getStringCellValue()));
            // 是否热卖
            resources.setIsHot(Integer.parseInt(row.getCell(16).getStringCellValue()));
            // 是否优惠
            resources.setIsBenefit(Integer.parseInt(row.getCell(17).getStringCellValue()));
            // 是否精品
            resources.setIsBest(Integer.parseInt(row.getCell(18).getStringCellValue()));
            // 是否新品
            resources.setIsNew(Integer.parseInt(row.getCell(19).getStringCellValue()));
            // 是否包邮
            resources.setIsPostage(Integer.parseInt(row.getCell(20).getStringCellValue()));
            // 是否优选
            resources.setIsGood(Integer.parseInt(row.getCell(21).getStringCellValue()));
            // 获得积分
            resources.setGiveIntegral(new BigDecimal(Double.parseDouble(row.getCell(22).getStringCellValue())));

            resources.setFicti(Integer.parseInt(row.getCell(23).getStringCellValue()));
            // 是否上架
            resources.setIsShow(Integer.parseInt(row.getCell(24).getStringCellValue()));

            List<PushMallSystemUserLevel> pushMallSystemUserLevel = pushMallSystemUserLevelRepository.findAll();
            PushMallStoreDiscount pushMallStoreDiscount = new PushMallStoreDiscount();
            pushMallStoreDiscount.setStartTime(OrderUtil.
                    dateToTimestamp(new Date()));
            pushMallStoreDiscount.setStartTimeDate(new Date());
            try {
                Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2029-01-01 00:00:00");
                pushMallStoreDiscount.setStopTime(OrderUtil.dateToTimestamp(parse));
                pushMallStoreDiscount.setEndTimeDate(parse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            pushMallStoreDiscount.setAddTime(OrderUtil.getSecondTimestampTwo());
            pushMallStoreDiscount.setStatus(1);
            pushMallStoreDiscount.setIsDel(0);
            pushMallStoreDiscount.setTitle(row.getCell(3).getStringCellValue());

            List discountList = new ArrayList();
            for (int m = 0; m < pushMallSystemUserLevel.size(); m++) {
                Map discountMap = new HashMap();
                Integer discount = Integer.parseInt(row.getCell(28 + m + 1).getStringCellValue());
                PushMallSystemUserLevel level = pushMallSystemUserLevel.get(m);
                discountMap.put("id", level.getId());
                discountMap.put("name", level.getName());
                discountMap.put("grade", level.getGrade());
                discountMap.put("discount", discount);
                discountList.add(discountMap);
            }

            for (int m = 0; m < pushMallSystemUserLevel.size(); m++) {
                Integer discount = Integer.parseInt(row.getCell(28 + m + 1).getStringCellValue());
                pushMallStoreDiscount.setId((int) (Math.random() * (352324 + 1)));
                pushMallStoreDiscount.setProductId(product_id);
                pushMallStoreDiscount.setGrade(pushMallSystemUserLevel.get(m).getGrade());
                pushMallStoreDiscount.setDiscount(discount);
                pushMallStoreDiscount.setJsonStr(JSON.toJSONString(discountList));
                pushMallStoreDiscountRepository.save(pushMallStoreDiscount);
            }

            String[] guige = row.getCell(5).getStringCellValue().split("/");
            String[] shuxing = row.getCell(6).getStringCellValue().split("/");
            // 商品编码
            String[] barCode = row.getCell(7).getStringCellValue().split("/");
            // 包装规格
            String[] packaging = row.getCell(8).getStringCellValue().split("/");
            // 成本价
            String[] cost = row.getCell(25).getStringCellValue().split("/");
            // 基价
            String[] num = row.getCell(26).getStringCellValue().split("/");
            String[] stock = row.getCell(27).getStringCellValue().split("/");
            String[] pic = row.getCell(28).getStringCellValue().split("\\&\\*\\&");

            Map map = new HashMap();
            List attrList = new ArrayList();
            for (int a = 0; a < guige.length; a++) {
                Map attrMap = new HashMap();
                attrMap.put("attrHidden", true);
                attrMap.put("detailValue", "");
                List shuxingList = Arrays.asList(shuxing[a].split(","));
                JSONArray jsonArray = new JSONArray();
                try {
                    jsonArray = JSONArray.parseArray(JSON.toJSONString(shuxingList));
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品属性不能包含特殊字符或不能为空 " + " 请检查后重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }
                attrMap.put("detail", shuxingList != null ? jsonArray : new ArrayList());
                attrMap.put("value", guige[a]);
                attrList.add(attrMap);
            }
            map.put("items", attrList);
            map.put("attrs", new ArrayList());
            DetailDTO detailDTO = attrFormat(JSON.toJSONString(map));

            List<ProductFormatDTO> newList = new ArrayList<>();
            for (int b = 0; b < detailDTO.getRes().size(); b++) {
                Map<String, Map<String, String>> valueMap = detailDTO.getRes().get(b);
                // Map<String, Map<String,String>> valueMap : detailDTO.getRes()
                ProductFormatDTO productFormatDTO = new ProductFormatDTO();
                productFormatDTO.setDetail(valueMap.get("detail"));

                try {
                    productFormatDTO.setPackaging(packaging[b].equals("") ? "0" : packaging[b]);
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 包装规格填写异常, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }

                try {
                    productFormatDTO.setBarCode(barCode[b].equals("") ? "0" : barCode[b]);
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品编码填写异常, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }

                try {
                    productFormatDTO.setCost(cost[b].equals("") ? new Double(0) : new Double(cost[b]));
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品成本价填写异常, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }

                try {
                    Double price = new Double(cost[b]) * new Double(num[b]);
                    productFormatDTO.setPrice(num[b].equals("1") ? new Double(0.00) : new Double(price));
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品零售价基数填写异常, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }

                try {
                    productFormatDTO.setSales(stock[b].equals("") ? new Integer(0) : Integer.valueOf(stock[b]));
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品库存填写异常, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }

                try {
                    productFormatDTO.setPic(pic[b].equals("") ? "" : pic[b]);
                } catch (Exception e) {
                    e.printStackTrace();
                    String msg = " 商品图片填写异常, " + " 请重新填写再次上传！ ";
                    throw new BadRequestException(msg);
                }

                productFormatDTO.setWholesale(new Double(0));
                productFormatDTO.setCheck(false);
                newList.add(productFormatDTO);
            }
            map.put("attrs", newList);

            // 添加时间
            resources.setAddTime(dateToTimestamp(new Date()));
            // 商品图片
            resources.setImage("@");
            // 商品轮播图
            resources.setSliderImage("@");
            resources.setCodePath("@");
            resources.setPrice(new BigDecimal(0));
            // 商品价格
            resources.setPrice(new BigDecimal(0));
            // 市场价
            resources.setOtPrice(new BigDecimal(0));
            // 成本价
            resources.setCost(new BigDecimal(0));
            // 库存
            resources.setStock(0);

            boolean existsById = pushMallStoreProductRepository.existsById(product_id);
            if (existsById) {
                pushMallStoreProductRepository.updatePrice(resources);
                List<PushMallStoreProductDTO> pushMallStoreProductDTOS = pushMallStoreProductMapper.toDto(pushMallStoreProductRepository.findAll());
                if (ObjectUtil.isNull(pushMallStoreProductDTOS)) {
                    String msg = " 商品信息修改失败! ";
                    throw new BadRequestException(msg);
                }
            } else {
                PushMallStoreProductDTO pushMallStoreProductDTO = pushMallStoreProductMapper.toDto(pushMallStoreProductRepository.save(resources));
                if (ObjectUtil.isNull(pushMallStoreProductDTO)) {
                    String msg = " 商品信息保存失败! ";
                    throw new BadRequestException(msg);
                }
            }
            createProductAttr(product_id, JSON.toJSONString(map));
        }
        return 1;
    }

    @Override
    public List<PushMallStoreProductDTO> findByIds(List<String> idList) {
        return pushMallStoreProductMapper.toDto(pushMallStoreProductRepository.findByIds(idList));
    }

    /**
     * 组合规则属性算法
     *
     * @param jsonStr
     * @return
     */
    public DetailDTO attrFormat(String jsonStr) {
        // List<Object> returnList = new ArrayList<>();

        JSONObject jsonObject = JSON.parseObject(jsonStr);
        List<FromatDetailDTO> fromatDetailDTOList = JSON.parseArray(jsonObject.get("items").toString(),
                FromatDetailDTO.class);
        List<String> data = new ArrayList<>();
        //List<Map<String,List<Map<String,String>>>> res =new ArrayList<>();
        List<Map<String, Map<String, String>>> res = new ArrayList<>();

        if (fromatDetailDTOList.size() > 1) {
            for (int i = 0; i < fromatDetailDTOList.size() - 1; i++) {
                if (i == 0) {
                    data = fromatDetailDTOList.get(i).getDetail();
                }
                List<String> tmp = new LinkedList<>();
                for (String v : data) {
                    for (String g : fromatDetailDTOList.get(i + 1).getDetail()) {
                        String rep2 = "";
                        if (i == 0) {
                            rep2 = fromatDetailDTOList.get(i).getValue() + "_" + v + "-"
                                    + fromatDetailDTOList.get(i + 1).getValue() + "_" + g;
                        } else {
                            rep2 = v + "-"
                                    + fromatDetailDTOList.get(i + 1).getValue() + "_" + g;
                        }

                        tmp.add(rep2);

                        if (i == fromatDetailDTOList.size() - 2) {
                            // Map<String,List<Map<String,String>>> rep4 = new LinkedHashMap<>();
                            Map<String, Map<String, String>> rep4 = new LinkedHashMap<>();
                            //List<Map<String,String>> listMap = new ArrayList<>();
                            //Map<String,String> map1 = new LinkedHashMap<>();
                            Map<String, String> reptemp = new LinkedHashMap<>();
                            for (String h : Arrays.asList(rep2.split("-"))) {
                                List<String> rep3 = Arrays.asList(h.split("_"));

                                if (rep3.size() > 1) {
                                    reptemp.put(rep3.get(0), rep3.get(1));
                                } else {
                                    reptemp.put(rep3.get(0), "");
                                }
                                //listMap.add(reptemp);

                            }
                            rep4.put("detail", reptemp);

                            //rep4.put("detail",listMap);

                            res.add(rep4);
                        }
                    }

                }

                //System.out.println("tmp:"+tmp);
                if (!tmp.isEmpty()) {
                    data = tmp;
                }
            }
        } else {
            List<String> dataArr = new ArrayList<>();

            for (FromatDetailDTO fromatDetailDTO : fromatDetailDTOList) {

                for (String str : fromatDetailDTO.getDetail()) {
                    Map<String, Map<String, String>> map2 = new LinkedHashMap<>();
                    //List<Map<String,String>> list1 = new ArrayList<>();
                    dataArr.add(fromatDetailDTO.getValue() + "_" + str);
                    Map<String, String> map1 = new LinkedHashMap<>();
                    map1.put(fromatDetailDTO.getValue(), str);
                    //list1.add(map1);
                    map2.put("detail", map1);
                    res.add(map2);
                }
            }

            String s = StrUtil.join("-", dataArr);
            data.add(s);
        }


        DetailDTO detailDTO = new DetailDTO();
        detailDTO.setData(data);
        detailDTO.setRes(res);


        return detailDTO;
    }

}
