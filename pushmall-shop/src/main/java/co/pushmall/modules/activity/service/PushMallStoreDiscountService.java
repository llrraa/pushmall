package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreDiscount;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

//@CacheConfig(cacheNames = "PushMallStoreDiscount")
public interface PushMallStoreDiscountService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreDiscountQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreDiscountDTO> queryAll(PushMallStoreDiscountQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreDiscountDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreDiscountDTO create(PushMallStoreDiscount resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreDiscount resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    PushMallStoreDiscountDTO findByProductId(Integer id);

}
