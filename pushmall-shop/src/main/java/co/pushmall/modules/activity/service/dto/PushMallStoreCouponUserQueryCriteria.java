package co.pushmall.modules.activity.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-10
 */
@Data
public class PushMallStoreCouponUserQueryCriteria {

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String couponTitle;
}
