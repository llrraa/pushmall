package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreSeckill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-12-14
 */
public interface PushMallStoreSeckillRepository extends JpaRepository<PushMallStoreSeckill, Integer>, JpaSpecificationExecutor {
}
