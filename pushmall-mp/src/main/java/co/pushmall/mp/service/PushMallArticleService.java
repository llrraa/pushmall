package co.pushmall.mp.service;


import co.pushmall.mp.domain.PushMallArticle;
import co.pushmall.mp.service.dto.PushMallArticleDTO;
import co.pushmall.mp.service.dto.PushMallArticleQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-07
 */
//@CacheConfig(cacheNames = "yxArticle")
public interface PushMallArticleService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallArticleQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallArticleDTO> queryAll(PushMallArticleQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallArticleDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallArticleDTO create(PushMallArticle resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallArticle resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    void uploadNews(PushMallArticleDTO yxArticleDTO) throws Exception;

}
